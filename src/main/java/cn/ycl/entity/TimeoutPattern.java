package cn.ycl.entity;

/**
 * @author yuanchangliang
 * @date 2023/11/6 16:01
 */
public class TimeoutPattern {
    public static void main(String[] args) {
        int timeout = 123;
        String str = "\"userType\":\"AntCloudIam\"},\"timeout\":30,\"triggerDetail\":\"* */1 * * ?\",\"triggerType\":\"timing\",\"updateTime\":1696746612000,\"versionPattern\":\"*\"},\"tagsList\":[]}";
        String str2 = "This is a timeout abc, and this is some other content.";
        String result = str.replaceAll("timeout:[^,]*,", ":" + String.valueOf(timeout));;
        System.out.println(result);
    }
}
