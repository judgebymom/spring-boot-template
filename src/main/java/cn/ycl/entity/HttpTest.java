package cn.ycl.entity;

import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * @author yuanchangliang
 * @date 2023/10/13 15:23
 */
public class HttpTest {
    public static void main(String[] args) {
        String url = "http://100.83.19.230:8888";
        String apiPrefix = "artifact/api/v1";
        String name = "";
        String protocolType = "Maven2";
        String repositoryType = "HOST";
        String userName = "system@service";
        String userToken = "dfdb907bc9e5a8e85eadeef60694ef8f";

        String requestUrl = String.format("%s/%s/repositories?pageNo=%s&pageSize=%s", url, apiPrefix,1,20);
        if(Objects.nonNull(name)){
            requestUrl += "&name=" + name;
        }
        if(Objects.nonNull(protocolType)){
            requestUrl += "&protocolType=" + protocolType;
        }
        if(Objects.nonNull(repositoryType)){
            requestUrl += "&repositoryType=" + repositoryType;
        }
        String response = httpGet(requestUrl, userName, userToken, 3);
        System.out.println("response:" + response);


    }

    public static String httpGet(String url, String userName, String token, int timeout) {
        CloseableHttpResponse response = null;
        try (CloseableHttpClient httpClient = newHttpClient(timeout)) {
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("ARTIFACT_ACCESS_NAME", userName);
            httpGet.setHeader("ARTIFACT_ACCESS_TOKEN", token);
            response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED || response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                // 查询数据成功
                InputStream responseContent = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(responseContent, StandardCharsets.UTF_8));
                StringBuilder sb = new StringBuilder();
                String strBlock;
                while ((strBlock = reader.readLine()) != null) {
                    sb.append(strBlock).append("\n");
                }
                if (sb.length() > 0 && sb.charAt(sb.length() - 1) == '\n') {
                    sb.setLength(sb.length() - 1);
                }
                return sb.toString();
            } else {
                String reasonPhrase = response.getStatusLine().getReasonPhrase();
                if(Objects.nonNull(reasonPhrase) && "server error".equalsIgnoreCase(reasonPhrase)){
                    reasonPhrase = String.format("user %s with token %s is unauthorized", userName, token);
                }
                return reasonPhrase;
            }
        } catch (Exception e) {

        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {

                }
            }
        }
        return null;
    }

    public static CloseableHttpClient newHttpClient(int timeout) {
        RequestConfig.Builder requestBuilder = RequestConfig.custom();
        requestBuilder.setConnectTimeout(timeout * 1000);
        requestBuilder.setConnectionRequestTimeout(timeout * 1000);
        requestBuilder.setSocketTimeout(timeout * 1000);
        HttpClientBuilder builder = HttpClientBuilder.create();
        builder.setDefaultRequestConfig(requestBuilder.build());
        return builder.build();
    }
}
