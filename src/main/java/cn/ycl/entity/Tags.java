package cn.ycl.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author yuanchangliang
 * @date 2023/7/11 10:31
 */
@Getter
@Setter
public class Tags {
    /**
     * 主键
     */
    private Long id;

    /**
     * 标签内容
     */
    private String content;

    /**
     * 标签颜色
     */
    private String color;

    /**
     * 标签范围
     */
    private String scope;

    public static void main(String[] args) {
        try{
            tryCat();
            System.out.println("abc");
        }catch (Exception e){
            System.out.println("-------------" + e);
        }
    }

    public static void tryCat(){

        try {
            if(System.currentTimeMillis() > 1000L){
                throw new RuntimeException("报错了");
            }
        } catch (RuntimeException e) {
            System.out.println("========" + e);
        }
    }

}
