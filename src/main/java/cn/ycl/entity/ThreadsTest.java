package cn.ycl.entity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.*;

/**
 * @author yuanchangliang
 * @date 2023/11/1 10:44
 */
@Slf4j
@Component
public class ThreadsTest {
    static int count = 0;
    BlockingQueue<String> actionQueue = new ArrayBlockingQueue<>(20);

    @PostConstruct
    private void consume() {
        ExecutorService consumeThread = Executors.newSingleThreadExecutor();
        consumeThread.submit(() -> {
            while (true) {
                String model;
                model = actionQueue.poll(2, TimeUnit.SECONDS);
                if (Objects.isNull(model)) {
                    TimeUnit.SECONDS.sleep(1);
                } else {
                    Runnable r = () -> {
                        try {
                            log.info(count + " begin:" + Thread.currentThread().getName() + " 输出：" + model + " 时间为：" + new Date());
                            log.info(count + " end:" + Thread.currentThread().getName() + " 输出：" + model + " 时间为：" + new Date());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    };
                    executeAction(r);
                }
            }
        });
    }

    protected void executeAction(Runnable r) throws ExecutionException, InterruptedException {
        /* 向线程池提交任务 */
        try {
            Future<?> submit = ThreadHelper.getExecutor().submit(r);
            Object result = submit.get();
            log.info(count + " Current thread get:" + Thread.currentThread().getName() + "时间为：" + new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

@Component
class ThreadHelper {

    private static ExecutorService executor;

    @PostConstruct
    public void init() {
        int threadSize = 2;
        int queueCapacity = 10;
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(queueCapacity);
        executor = new ThreadPoolExecutor(
                threadSize,
                threadSize,
                0L,
                TimeUnit.MILLISECONDS,
                queue,
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
    }

    public static ExecutorService getExecutor() {
        return executor;
    }
}
