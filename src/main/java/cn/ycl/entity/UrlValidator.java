package cn.ycl.entity;

public class UrlValidator {
    public static void main(String[] args) {
        String url = "11.11.1111.11";
        boolean isIp = isIpAddress(url);
        System.out.println(isIp);
    }

    public static boolean isIpAddress(String input) {
        String[] parts = input.split("\\.");
        if (parts.length != 4) {
            return false;
        }

        for (String part : parts) {
            try {
                int number = Integer.parseInt(part);
                if (number < 0 || number > 255) {
                    return false;
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }

        return true;
    }
}