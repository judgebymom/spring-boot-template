package cn.ycl.entity;

import java.util.regex.Pattern;

/**
 * @author yuanchangliang
 * @date 2023/10/25 13:58
 */
public class RegexTest {
    public static void main(String[] args) {
        String patternStr = "org.apache.commons:commons-lang3";
        String pattern = "^org.apache.commons.*";
        boolean matches = Pattern.matches(pattern, patternStr);
        System.out.println("第一种情况，匹配以org.apache.commons开头的：" + matches);

        String pattern1 = ".*commons-lang3$";
        boolean matches1 = Pattern.matches(pattern1, patternStr);
        System.out.println("第二种情况，匹配以commons-lang3结尾的：" + matches1);

        String pattern2 = ".*commons.*";
        boolean matches2 = Pattern.matches(pattern2, patternStr);
        System.out.println("第三种情况，匹配以带commons的：" + matches2);

        //换个匹配字符串，看是否返回预期
        String patternStr2 = "1org.apache.commons1";
        //预期为false
        System.out.println(Pattern.matches(pattern, patternStr2));
        //预期为false
        System.out.println(Pattern.matches(pattern1, patternStr2));
        //预期为true
        System.out.println(Pattern.matches(pattern2, patternStr2));


    }
}
