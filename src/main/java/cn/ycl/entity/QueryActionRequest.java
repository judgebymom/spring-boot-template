package cn.ycl.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

/**
 * @author yuanchangliang
 * @date 2023/8/17 18:14
 */
@Getter
@Setter
public class QueryActionRequest {
    /**
     * 同步策略id
     */
    private Long policyId;

    /**
     * pageNo
     */
    private Integer pageNo;

    /**
     * pageSize
     */
    private Integer pageSize;
    /**
     * 制品名称
     */
    private String name;
    /**
     * 制品版本
     */
    private String version;
    /**
     * 触发方式
     */
    private String triggerType;

    /**
     * 同步状态
     */
    private String actionStatus;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    public Integer getPageNo() {
        return Objects.isNull(pageNo) ? 1 : pageNo;
    }

    public Integer getPageSize() {
        return Objects.isNull(pageNo) ? 10 : pageNo;
    }
}
