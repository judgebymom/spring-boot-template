package cn.ycl.entity;

import java.util.HashMap;
import java.util.Objects;

public class StringReplaceExample {
    public static void main(String[] args) {
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("a","b");
        String c = stringStringHashMap.get("c");
        String path = "${env}/${dd}/${appname}/${branch}/${hhhhh}/${env}/${tenant}/${stage}/Dockerfile";
        path = path.replaceAll("\\$\\{appname}","ycl-test-app");
        path = path.replaceAll("\\$\\{branch}","*/ECC10000000");
        path = path.replaceAll("\\$\\{env}","DEV");
        path = path.replaceAll("\\$\\{tenant}", Objects.isNull(c)? "null":c);
        path = path.replaceAll("\\$\\{stage}","开发阶段");

        String result = path.replaceAll("\\$\\{\\w+}", "null");
        System.out.println(result);
    }
}