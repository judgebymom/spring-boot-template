package cn.ycl.entity;


import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * artifact path item
 *
 * @author twb
 */
@Getter
@Setter
public class DockerimageProject {

    /**
     * 同步地址的name字段
     */
    private String syncSiteId;

    /**
     * repositoryType
     */
    private String repositoryType;

    /**
     * chart_count
     */
    private int chart_count;

    /**
     * 创建时间
     */
    private String creation_time;

    /**
     * 是否公开
     */

    private Metadata metadata;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 项目负责人
     */
    private String owner_name;

    /**
     * repo_count
     */
    private String repo_count;

    /**
     * 修改时间
     */
    private String update_time;


    /**
     * 标签列表
     */
    private List<Tags> tagsList;

    /**
     * 备注
     */
    private String remark;

}
