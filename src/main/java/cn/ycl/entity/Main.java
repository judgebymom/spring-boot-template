package cn.ycl.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String jsonArrayStr = "[{\"syncSiteId\":\"test-ycl\",\"repositoryType\":null,\"chart_count\":0,\"creation_time\":\"2023-07-27T16:41:17.495+08:00\",\"metadata\":{\"projectPublic\":null},\"name\":\"abc\",\"owner_name\":\"admin\",\"repo_count\":\"0\",\"update_time\":\"2023-07-27T16:41:17.495+08:00\",\"tagsList\":[],\"remark\":null}]";

        JSONArray jsonArray = JSONArray.parseArray(jsonArrayStr);

        List<DockerimageProject> projects = new ArrayList<>();
        for (Object o : jsonArray) {
            DockerimageProject dockerimageProject = (DockerimageProject) o;
        }
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            DockerimageProject project = jsonObject.toJavaObject(DockerimageProject.class);
            projects.add(project);
        }

        for (DockerimageProject project : projects) {
            System.out.println(project);
        }
    }
}


class Metadata {
    private String projectPublic;

    // Getters and Setters

    @Override
    public String toString() {
        return "Metadata{" +
                "projectPublic='" + projectPublic + '\'' +
                '}';
    }
}

