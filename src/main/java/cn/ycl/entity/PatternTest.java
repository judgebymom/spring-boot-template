package cn.ycl.entity;

import java.util.Objects;

/**
 * @author yuanchangliang
 * @date 2023/10/27 17:06
 */
public class PatternTest {
    public static void main(String[] args) {
        String artifactPath = "test/test-12.jar";
//        String artifactPath = "name/name-1.0.0.tar.gz";
        // 两种模式，兼容路径分段中带版本 /${version}/ 和 不带版本的两种格式
        // 解决无法区分后缀和版本号的分界的问题：
        // 如：name/name-1.0.0.tar.gz，难以区分版本号是 1.0.0 还是 1.0.0.tar
        // mode1，不带版本
        PatternTest patternTest = new PatternTest();
        CommonArtifactIndex index = patternTest.parseIndexFromArtifactPathV1(artifactPath);
        // mode2，带版本
        if (index == null) {
            index = patternTest.parseIndexFromArtifactPathV2(artifactPath);
        }
        // mode3，完全自定义，直接取最后的分段，去除后缀后作为name
        if (index == null) {
            index = patternTest.parseIndexFromArtifactPathCustom(artifactPath);
        }
        if (index == null) {
            throw new IllegalArgumentException("大错特错");
        }
        System.out.println("path:" + index.getPath());
        System.out.println("name:" + index.getName());
        System.out.println("version:" + index.getVersion());

        System.out.println("=======================");
        CommonArtifactIndex commonArtifactIndex = patternTest.parseIndexFromName("ycl-equals-50m.1.jar", null);

        System.out.println("path:" + commonArtifactIndex.getPath());
        System.out.println("name:" + commonArtifactIndex.getName());
        System.out.println("version:" + commonArtifactIndex.getVersion());


    }

    CommonArtifactIndex parseIndexFromName(String name, String metaJson) {
        CommonArtifactIndex index = new CommonArtifactIndex();
        index.setName(name);
        //根据最后一个‘-’ 切割
        String[] comps = name.split("-(?=[^-]*$)");
        if (comps.length < 2) {
            //同步不限制制品名称规范性
            if(Objects.nonNull(metaJson)){
                return index;
            }
            return null;
        }
        String versionAndSuffix = name.replace(comps[0] + "-", "");
        //根据最后一个‘.’ 切割
        String[] comps2 = versionAndSuffix.split("\\.(?=[^.]*$)");
        if (comps2.length < 2) {
            //同步不限制制品名称规范性
            if(Objects.isNull(metaJson)){
                return null;
            }
        }
        String version = comps2[0];
        index.setVersion(version);
        return index;
    }

    CommonArtifactIndex parseIndexFromArtifactPathV1(String artifactPath) {
        System.out.println("11111111111111111111");
        String[] comps = artifactPath.split("/");
        if (comps.length < 2) {
            return null;
        }
        String name = comps[comps.length - 1];
        String version = null;
        String fileComp = comps[comps.length - 1];
        String fileExt = null;
        if (!fileComp.startsWith(name)) {
            return null;
        }
        if (fileComp.contains(".")) {
            fileExt = fileComp.substring(fileComp.lastIndexOf(".") + 1);
        }
        if (fileExt == null || fileExt.equals("")) {
            return null;
        }
        if (fileComp.contains(String.format("%s-", name))) {
            version = fileComp.substring(name.length() + 1, fileComp.lastIndexOf(fileExt) - 1);
        }
        CommonArtifactIndex index = new CommonArtifactIndex();
        index.setPath(artifactPath);
        index.setName(name);
        index.setVersion(version);
        return index;
    }

    /**
     * parse artifact path v2
     * artifact path format: [${dir1}/${dir2}/...]/${name}/${version}/${name}[-${version}].${file_ext}
     *
     * @param artifactPath artifact path
     * @return index
     */
    CommonArtifactIndex parseIndexFromArtifactPathV2(String artifactPath) {
        System.out.println("2222222222222222222222222222");
        String[] comps = artifactPath.split("/");
        if (comps.length < 3) {
            return null;
        }
        String name = comps[comps.length - 3];
        String version = comps[comps.length - 2];

        String nameVersionPrefix = String.format("%s-%s.", name, version);
        String fileComp = comps[comps.length - 1];
        if (!fileComp.startsWith(nameVersionPrefix)) {
            return null;
        }
        String fileExt = fileComp.substring(nameVersionPrefix.length());
        if (fileExt == null || fileExt.equals("")) {
            return null;
        }
        CommonArtifactIndex index = new CommonArtifactIndex();
        index.setPath(artifactPath);
        index.setName(name);
        index.setVersion(version);
        return index;
    }

    /**
     * parse custom artifact path， no version
     *
     * @param artifactPath artifact path
     * @return index
     */
    CommonArtifactIndex parseIndexFromArtifactPathCustom(String artifactPath) {
        System.out.println("3333333333333333333333333");
        CommonArtifactIndex index = new CommonArtifactIndex();
        index.setPath(artifactPath);

        String[] comps = artifactPath.split("/");
        if (comps.length == 0) {
            return null;
        }
        String fileComp = comps[comps.length - 1];

        index.setName(fileComp.contains(".") ? fileComp.substring(0, fileComp.lastIndexOf(".")) : fileComp);
        index.setVersion(null);
        return index;
    }

    class CommonArtifactIndex{
        String path;
        String name;
        String version;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }

}
