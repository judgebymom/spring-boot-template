package cn.ycl.entity;

/**
 * @author yuanchangliang
 * @date 2023/11/20 9:45
 */
public class CheckIpExist {
    public static void main(String[] args) {
        boolean ipAddressInSubnet = isIpAddressInSubnet("100.88.142.174","255.255.255.0");
        System.out.println(ipAddressInSubnet);
        System.out.println("CheckIpExist4444");
    }

    public static boolean isIpAddressInSubnet(String ipAddress, String subnetRange) {
        String[] networkips = ipAddress.split("\\.");
        int ipAddr = (Integer.parseInt(networkips[0]) << 24)
                | (Integer.parseInt(networkips[1]) << 16)
                | (Integer.parseInt(networkips[2]) << 8)
                | Integer.parseInt(networkips[3]);

        // 拿到主机数
        int type = Integer.parseInt(subnetRange.replaceAll(".*/", ""));
        int ipCount = 0xFFFFFFFF << (32 - type);

        String maskIp = subnetRange.replaceAll("/.*", "");
        String[] maskIps = maskIp.split("\\.");

        int cidrIpAddr = (Integer.parseInt(maskIps[0]) << 24)
                | (Integer.parseInt(maskIps[1]) << 16)
                | (Integer.parseInt(maskIps[2]) << 8)
                | Integer.parseInt(maskIps[3]);

        return (ipAddr & ipCount) == (cidrIpAddr & ipCount);
    }
}
