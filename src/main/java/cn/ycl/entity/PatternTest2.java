package cn.ycl.entity;

/**
 * @author yuanchangliang
 * @date 2023/10/30 16:04
 */
public class PatternTest2 {
    public static void main(String[] args) {
        String artifactPath = "name/name-1.0.0.tar.gz";
        //不再支持url中带version的情况，
        String name = null;
        String version = null;
        CommonArtifactIndex index = new CommonArtifactIndex();
        index.setPath(artifactPath);
        if (artifactPath.contains("/")) {
            String[] pathSplit = artifactPath.split("/");
            name = pathSplit[pathSplit.length - 1];
            if (name.contains("-")) {
                String[] nameSplit = name.split("-");
                version = nameSplit[nameSplit.length - 1];
                version = version.substring(0, version.lastIndexOf("."));
            }
            index.setName(name);
            index.setVersion(version);
        } else {
            throw new IllegalArgumentException("artifact path is not available, correct format: [${dir1}/${dir2}/...]/${name}/[${version}/]${name}[-${version}].${file_ext}");
        }
        System.out.println("path:" + artifactPath);
        System.out.println("name:" + name);
        System.out.println("version:" + version);
    }


}
class CommonArtifactIndex{
    String path;
    String name;
    String version;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}

