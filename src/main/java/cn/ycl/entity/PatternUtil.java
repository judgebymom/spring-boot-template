package cn.ycl.entity;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PatternUtil {
    public static final String PATTERN_ALL = "*";
    public static List<String> list = new ArrayList<>();

    public static boolean matchPatterns(String value, String pattern) {
        if (StringUtils.isEmpty(pattern) || PATTERN_ALL.equals(pattern) || value == null) {
            return true;
        }

        boolean matched = false;
        try {
            matched = Pattern.matches(pattern, value);
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(value + "---"+ pattern +"---"+ matched);
        return matched;
    }

    public static void main(String[] args) {
        list.add("abcfdfdfd");
        list.add("111fdf222");
        list.add("111fdf2223");
        list.add("11cfdfd23");
        list.add("123fdf123");
        String match = "111*";
        for (String s : list) {
            if(matchPatterns(s,match)){
                System.out.println(s);
            }
        }
    }
}
