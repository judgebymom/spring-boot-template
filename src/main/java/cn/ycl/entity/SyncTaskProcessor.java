package cn.ycl.entity;

import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yuanchangliang
 * @date 2023/11/8 9:53
 */
public class SyncTaskProcessor {
    static BlockingQueue<SyncTask> actionQueue = new ArrayBlockingQueue<>(20);
    static ConcurrentHashMap<String, Integer> targetMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        //假设actionQueue已经有10个SyncTask
        SyncTask task = actionQueue.poll();
        String target = task.getTarget();
        Integer param = task.getParam();
        Integer para = targetMap.get(target);
        if(Objects.nonNull(para)){
            if(!param.equals(para)){
                targetMap.put(target,param);
                //重新为target类型的SyncTask创建新的param大小的线程池
            }
        }
        //根据不同的target，为SyncTask选择不同的线程池执行，注意内存泄露


    }
}


class SyncTask {
    private String target;
    private int param;

    public SyncTask(String target, int param) {
        this.target = target;
        this.param = param;
    }

    public String getTarget() {
        return target;
    }
    public Integer getParam() {
        return param;
    }

}