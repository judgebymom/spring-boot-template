package cn.ycl.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse {
    private boolean success;
    private String msg;
    private Object data;

    // 构造方法
    public ApiResponse(boolean success, String msg, Object data) {
        this.success = success;
        this.msg = msg;
        this.data = data;
    }

    // Getters and Setters
    // ...
}