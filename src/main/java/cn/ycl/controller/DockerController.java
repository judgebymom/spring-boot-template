package cn.ycl.controller;

/**
 * @author yuanchangliang
 * @date 2023/8/31 14:12
 */

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;

@RestController
public class DockerController {



    @PostMapping("/config")
    public ResponseEntity<String> handleConfig(@RequestBody ConfigRequest config) {
        try {
            FileWriter authWriter = new FileWriter("auth.json");
            authWriter.write(config.getAuth());
            authWriter.close();

            FileWriter imagesWriter = new FileWriter("images.json");
            imagesWriter.write(config.getImages());
            imagesWriter.close();

            return new ResponseEntity<>("Configuration saved successfully", HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

class ConfigRequest {
    private String auth;
    private String images;

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
