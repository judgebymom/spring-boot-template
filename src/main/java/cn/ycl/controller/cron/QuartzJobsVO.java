package cn.ycl.controller.cron;

import lombok.Getter;
import lombok.Setter;

/**
 * @author yuanchangliang
 * @date 2023/8/17 11:14
 */
@Getter
@Setter
public class QuartzJobsVO {
    String jobDetailName;

    String jobCronExpression;

    String groupName;

    String status;
}
