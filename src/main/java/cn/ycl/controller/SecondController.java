package cn.ycl.controller;

import cn.ycl.controller.cron.QuartzJobsVO;
import cn.ycl.controller.cron.SimpleJob;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yuanchangliang
 * @date 2022-06-07 17:26
 **/
@RestController
@Slf4j
public class SecondController {

    @Autowired
    private Scheduler scheduler;

    private ExecutorService consumeThread;

    @Setter
    private boolean running = true;


    @GetMapping("/second")
    public String first(){
        Date date = new Date();
        if(date.getTime() > 1){
            throw new RuntimeException("错了错了");
        }
        System.out.println("laillllllllllllllllllllllllll");
        return "接口请求正常（尝试流水线重新部署）";
    }




    @PostConstruct
    public void initJob(){
//        createJob("job1","group1","0/5 * * * * ? ","trigger1","triggerGroup1");
//        createJob("job1","group1","0/2 * * * * ? ","trigger1","triggerGroup1");
//        createJob("job2","group2","0/10 * * * * ? ","trigger2","triggerGroup2");
//        createJob("job3","group3","0/3 * * * * ? ","trigger3","triggerGroup3");
    }

    @PostConstruct
    public void consume(){
        int interval = 5;
        Random random = new Random();
        long initialDelay = 10 + random.nextInt(10);

        ScheduledThreadPoolExecutor runExec = new ScheduledThreadPoolExecutor(1);
        runExec.scheduleWithFixedDelay(() -> {
            try {
//                log.info("===========开始了----------" + new Date());
            } catch (Exception e) {
                log.info("[Action Scheduler][actionProcessFailed] message:{}", e.getMessage());
            }

        }, initialDelay, interval, TimeUnit.SECONDS);

    }




    public void createJob(String jobName,String jobGroup,String cron,String triggerName,String triggerGroup) {
        try {
            JobKey jobKey = new JobKey(jobName, jobGroup);
            // 如果存在这个任务，则删除
            if (scheduler.checkExists(jobKey)) {
                scheduler.deleteJob(jobKey);
            }

            JobDetail jobDetail = JobBuilder.newJob(SimpleJob.class)
                    .withIdentity(jobKey)
                    .build();

            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cron);

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(triggerName, triggerGroup)
                    .withSchedule(cronScheduleBuilder).build();
            scheduler.scheduleJob(jobDetail, trigger);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @GetMapping("remove")
    public void removeJob(String jobName, String jobGroup,String triggerName,String triggerGroup) throws SchedulerException {

        TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        Trigger trigger =  scheduler.getTrigger(triggerKey);
        if (trigger == null) {
            return;
        }
        // 停止触发器
        scheduler.pauseTrigger(triggerKey);
        // 移除触发器
        scheduler.unscheduleJob(triggerKey);
        // 删除任务
        scheduler.deleteJob(jobKey);
    }

    @GetMapping("/getAllJobs")
    public List<QuartzJobsVO> getAllJob(){
        GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
        Set<JobKey> jobKeys = null;
        List<QuartzJobsVO> jobList = new ArrayList();
        try {
            jobKeys = scheduler.getJobKeys(matcher);
            for (JobKey jobKey : jobKeys) {
                List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
                for (Trigger trigger : triggers) {
                    QuartzJobsVO job = new QuartzJobsVO();
                    job.setJobDetailName(jobKey.getName());
                    job.setGroupName(jobKey.getGroup());
                    job.setJobCronExpression("触发器:" + trigger.getKey());
                    Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
                    job.setStatus(triggerState.name());
                    if (trigger instanceof CronTrigger) {
                        CronTrigger cronTrigger = (CronTrigger) trigger;
                        String cronExpression = cronTrigger.getCronExpression();
                        job.setJobCronExpression(cronExpression);
                    }
                    jobList.add(job);
                }
            }

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return jobList;

    }

}