package cn.ycl.controller;

import cn.ycl.entity.ApiResponse;
import cn.ycl.entity.QueryActionRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.quartz.CronExpression;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Objects;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 * @author yuanchangliang
 * @date 2022-06-07 17:26
 **/
@RestController
@Slf4j
public class FirstController {
    @GetMapping("/first")
    public String first(){
        Date date = new Date();
//        if(date.getTime() > 1){
//            throw new RuntimeException("错了错了");
//        }
        System.out.println("laillllllllllllllllllllllllll");
        return "接口请求正常（尝试流水线重新部署）";
    }

    @GetMapping("/abcd/{id}/2")
    @ResponseBody
    public ApiResponse first2(QueryActionRequest param,
                              @PathVariable("policyId") Long policyId){
        System.out.println(param.getName());
        System.out.println(policyId);
        return new ApiResponse(true,null,"不错");
    }



        @GetMapping("/example")
        public void exampleEndpoint() {
            try {
                print();
            } catch (Exception e) {
                System.out.println("ddddddddddddddddd");
                e.printStackTrace();
            }
        }

        public void print() throws Exception{
        String abc = "123";
            try {
                // 在这里执行可能会抛出异常的代码
                abc = "456";
                int result = 10 / 0; // 一个例子，除以零会抛出ArithmeticException

                // 如果没有异常抛出，可以返回成功的响应
                System.out.println("操作成功");
            } catch (Exception e) {
                // 捕获异常并返回错误响应
                abc = "879";
                throw new RuntimeException(abc);
            }finally {
                abc = "111";
                System.out.println(abc);
            }
        }


    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return "请选择要上传的文件";
        }

        try {
            // 获取文件名
            String fileName = file.getOriginalFilename();
            // 指定文件存储路径
            String filePath = "D:/others/";
            // 创建目标文件对象
            File dest = new File(filePath + fileName);
            // 将文件保存到目标位置
            file.transferTo(dest);

            // 读取Jar包的元数据
            JarFile jarFile = new JarFile(dest);
            Manifest manifest = jarFile.getManifest();
            Attributes attributes = manifest.getMainAttributes();

            // 获取版本号
            String version = attributes.getValue("Implementation-Version");
            // 获取Artifact ID
            String artifactId = attributes.getValue("Implementation-Title");
            // 获取Group ID
            String groupId = attributes.getValue("Implementation-Vendor-Id");

            // 打印版本号、Artifact ID和Group ID
            System.out.println("版本号: " + version);
            System.out.println("Artifact ID: " + artifactId);
            System.out.println("Group ID: " + groupId);

            return "文件上传成功";
        } catch (IOException e) {
            e.printStackTrace();
            return "文件上传失败";
        }
    }



    public static void main(String[] args) throws IOException {
        String abc = " 30 16 * * ? ";
        System.out.println(abc.startsWith(" ") + "-" +  abc.endsWith(" "));
        System.out.println(abc.split(" ").length);
    }

    public static byte[] downloadArtifactForSync(String downloadUrl) throws IOException {
        CloseableHttpResponse response = null;
        byte[] bytes = null;
        try {
            CloseableHttpClient httpClient = newHttpClient(3);
            HttpGet httpGet = new HttpGet(downloadUrl);
            response = httpClient.execute(httpGet);
            if (Objects.isNull(response) || response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new RuntimeException("hhh");
            }
            bytes = new byte[response.getEntity().getContent().available()];
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    System.out.println();
                }
            }
        }
        return bytes;
    }


    public static String convertTimeExpression(String expression) {
        StringBuilder convertedExpression = new StringBuilder();
        boolean foundNonWildcard = false;

        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if(foundNonWildcard){
                convertedExpression.append(c);
                continue;
            }
            if (c == '*') {
                convertedExpression.append('0');
            } else if (c == ' ') {
                convertedExpression.append(c);
            } else {
                convertedExpression.append(c);
                foundNonWildcard = true;
            }
        }

        return convertedExpression.toString();
    }



    public static boolean isValidCronExpression(String expression) {
        try {
            new CronExpression(expression);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static CloseableHttpClient newHttpClient(int timeout) {
        RequestConfig.Builder requestBuilder = RequestConfig.custom();
        requestBuilder.setConnectTimeout(timeout * 1000);
        requestBuilder.setConnectionRequestTimeout(timeout * 1000);
        requestBuilder.setSocketTimeout(timeout * 1000);
        HttpClientBuilder builder = HttpClientBuilder.create();
        builder.setDefaultRequestConfig(requestBuilder.build());
        return builder.build();
    }

    public static CloseableHttpResponse httpGetDownload(String url) {
        CloseableHttpResponse response = null;
        try (CloseableHttpClient httpClient = newHttpClient(3)) {
            HttpGet httpGet = new HttpGet(url);
            response = httpClient.execute(httpGet);
            return response;
        } catch (Exception e) {
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    public static String getResponse(CloseableHttpResponse response) {
        try {
            // 查询数据成功
            InputStream responseContent = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseContent, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            String strBlock;
            while ((strBlock = reader.readLine()) != null) {
                sb.append(strBlock).append("\n");
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == '\n') {
                sb.setLength(sb.length() - 1);
            }
            return sb.toString();
        } catch (Exception e) {
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }


    class Parent {
        public void methodA(B obj) {
            System.out.println("Parent: " + obj);
        }
    }

    class Child extends Parent {
        public void methodA(C obj) {
            System.out.println("Child: " + obj);
        }
    }

    class E extends Parent {
        public void methodA(F obj) {
            System.out.println("E: " + obj);
        }
    }
//写一个计算器
    class B {
        // ...
    }

    class C extends B {
        // ...
    }

    class F extends B {
        // ...
    }




}